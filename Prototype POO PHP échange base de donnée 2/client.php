<?php 
class Client {
    protected $id;
    protected $nom;
    protected $email;
    protected $annee;
    
function __construct($id, $n=null, $m=null, $a=null) {
    if($id==null){
        $this->id      = $id;
        $this->nom     = $n;
        $this->email   = $m;
        $this->annee   = $a;
    }
    else {
        $this->load($id);
    }
}

public function getId(){return $this->id;}
public function getNom(){return $this->nom;}
public function getEmail(){return $this->email;}
public function getAnnee(){return $this->annee;}
    

public function setNom($n){$this->nom=$n;}
public function setEmail($m){$this->email=$m;}
public function setAnnee($a){$this->annee=$a;}

    public function save() {
        require('BdD.php');

        $req = $bdd->prepare('INSERT INTO client (nom,email,annee) VALUES (?, ?, ?)');
        $req->bindParam(1, $this->nom);
        $req->bindParam(2, $this->email);
        $req->bindParam(3, $this->annee);
        $req->execute();
    
//$req->execute(array($_GET['nom'], $_GET['email'], $_GET['annee']));

}
    public function load($id) {
        require('BdD.php');

        $req = $bdd->prepare('SELECT * FROM client WHERE id_client=?');
        $req->bindParam(1, $id);
        $req->execute();
        $reponses=$req->fetch(PDO::FETCH_ASSOC);
        $this->id      = $reponses['id_client'];
        $this->nom     = $reponses['nom'];
        $this->email   = $reponses['email'];
        $this->annee   = $reponses['annee'];

    }
    public function update(){
        require('BdD.php');
        
        $req = $bdd->prepare('UPDATE client SET nom=?, email=?, annee=? WHERE id_client=?');
        $req->bindParam(1, $this->nom);
        $req->bindParam(2, $this->email);
        $req->bindParam(3, $this->annee);
        $req->bindParam(4, $this->id);
        $req->execute();
    }
}   

?>