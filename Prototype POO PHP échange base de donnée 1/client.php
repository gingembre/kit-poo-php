<?php 
class client {
    protected $id;
    protected $nom;
    protected $mail;
    protected $naissance; 

    function __construct($id, $n=null, $m=null, $a=null) {
        if($id == null){
            $this->id         = $id;
            $this->nom         = $n;
            $this->mail     = $m;
            $this->annee     = $a;
        }
        else {
            $this->load($id);
        }
    }

    public function getId() { return $this->id;}
    public function getNom() { return $this->nom;}
    public function getMail() { return $this->mail;}
    public function getAnnee() { return $this->annee;}

    public function setNom($n) { $this->nom=$n; }
    public function setMail($m) { $this->mail=$m; }
    public function setAnnee($a) { $this->annee=$a; }

    public function save() {
        require('modele.php');
        $req = $bdd->prepare('INSERT INTO Client(nom, mail, naissance) values (?,?,?)');
        $req->bindParam(1, $this->nom);
        $req->bindParam(2, $this->mail);
        $req->bindParam(3, $this->annee);
        $req->execute();

    }

    protected function load($id) {
        require('modele.php');
        $req1 = $bdd->prepare('SELECT * FROM Client WHERE ID = ?');
        $req1->bindParam(1, $id);
        $req1->execute();
        $fetch = $req1->fetch(PDO::FETCH_ASSOC);

        $this->id = $fetch['ID'];
        $this->nom = $fetch['nom'];
        $this->mail = $fetch['mail'];
        $this->annee = $fetch['naissance'];
    }

    static function getList(){
        require('modele.php');    
        $tabClient = array();

        $req2 = $bdd->prepare('SELECT ID FROM Client');
        $req2->execute();
        

        while($inf = $req2->fetch(PDO::FETCH_ASSOC)){
            array_push($tabClient, new Client($inf['ID']));
        }

        return $tabClient;
    }

    public function update(){
        require('modele.php');

        $req3 = $bdd->prepare('UPDATE Client SET nom = ?, mail = ?, naissance = ? WHERE ID = ?');
        $req3->bindParam(1, $this->nom);
        $req3->bindParam(2, $this->mail);
        $req3->bindParam(3, $this->annee);
        $req3->bindParam(4, $this->id);
        $req3->execute();

    }

    //Client::

    
}
?>