<?php
function AfficherHaut($titre='Ajouter un nouveau client') {
    echo ("
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset='utf-8'>
            <title></title>
            <link rel='stylesheet' type='text/css' href='form.css'>
        </head>
        <body>
            <header>
                <h1>".$titre."<h1>
                <form action='index.php' method='get' id='ajouter'>
                    <button type='submit' name='choix' value='ajouter'>Ajouter</button>
                </form>
            </header>
                <h2>voici les infos</h2>
        ");
}

function AfficherFormulaire($cl=null) {
    
    $id = $nom = $mail = $annee = "";
    
    if(null!==$cl){
        $id = $cl->getId();
        $nom = $cl->getNom();
        $mail = $cl->getMail();
        $annee =$cl->getAnnee();
    }

    echo ("
                <form action='index.php' method='get'>
                <input type='hidden' name='id' value='".$id."'/>
                    <div>
                        <label for='name'>Nom :</label>
                        <input type='text' id='name' name='nom' value='".$nom."'/>
                    </div>
                    <div>
                        <label for='mail'>e-mail :</label>
                        <input type='email' id='mail' name='mail' value='".$mail."'/>
                    </div>
                    <div>
                        <label for='year'>Date de naissance :</label>
                        <input type='text' id='year' name='annee' value='".$annee."'/>
                    </div>
                    <button type='submit' name='choix' value='enregister'>Envoyer</button>
                    <button type='submit' name='choix' value='annuler'>Annuler</button>
                </form></div>
        ");
}

function AfficherBas() {
    echo ("
            </div>
            <footer>
                <p>Ceci est le footer</p>
            </footer>
        </body>
        </html>
        ");
}

function AfficherInfos() {

    echo '<div id="box">
            <p id="infos">';

    $tab=Client::getList();

    foreach($tab as $info){

        if(null!==$info->getId()){
            echo     'Client n° '.$info->getId().' : <br />
                    Nom : '.$info->getNom().'<br />
                    Mail : '.$info->getMail().'<br />
                    Date de naissance : '.$info->getAnnee().'<br />
                    <a href="index.php?choix=modifier&id='.$info->getId().'">Modifier</a>
                    <br /><br />';
        }
    }
    echo '</p>';

}

?>