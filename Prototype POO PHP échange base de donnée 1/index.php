<?php

require('client.php');

require('vue.php');

AfficherHaut();


$choix = 'afficher';

if(isset($_REQUEST['choix'])){
    $choix = $_REQUEST['choix'];
}

switch($choix) {
    case 'ajouter' :     AfficherInfos();
                        AfficherFormulaire();

                        break;

    case 'enregister' : if($_REQUEST['id']!=""){ //modif
                            $cli = new Client($_REQUEST['id']);
                            $cli->setNom($_REQUEST['nom']);
                            $cli->setMail($_REQUEST['mail']);
                            $cli->setAnnee($_REQUEST['annee']);
                            $cli->update();
                        }
                        else{                    //nouveau
                            $cli = new Client(null, $_REQUEST['nom'], $_REQUEST['mail'], $_REQUEST['annee']);
                            $cli->save();
                        }
                        AfficherInfos();


                        break;

    case 'annuler' :    AfficherInfos();
                        break;

    case 'modifier' :    $cl = new Client($_REQUEST['id']);
                        AfficherInfos();
                        AfficherFormulaire($cl);
                        break;

    default :             break;
}

AfficherBas();
?>